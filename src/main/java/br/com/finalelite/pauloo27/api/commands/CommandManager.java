package br.com.finalelite.pauloo27.api.commands;

import br.com.finalelite.pauloo27.api.commands.usage.Usage;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;

/**
 * A RSpigot class.
 * Part of the command API. Register your commands, to get the current server's instance use
 * {@link Bukkit#getServer()#getCommandManager()}.
 *
 * @author Paulo
 * @version 1.0
 */
public class CommandManager {

    /**
     * Registers a new command.
     *
     * @param command The command
     */
    public void register(BaseCommand command) {
        Bukkit.getCommandMap().register(command.getFallback(), command);
    }

    /**
     * Registers a new command with the plugin default permission message, usage message, etc.
     *
     * @param command The command
     * @param plugin  The plugin
     */
    public static void register(BaseCommand command, CommandablePlugin plugin) {
        String fallback = command.getFallback() == null ? plugin.getDefaultFallback() : command.getFallback();
        Bukkit.getCommandMap().register(fallback, command);
        if (command.usage.getUsageMessageFormat() == null)
            command.usage.setUsageMessageFormat(plugin.getDefaultUsageMessage());
        if (command.getCannotRunMessage() == null)
            command.setCannotRunMessage(plugin.getDefaultCannotRunMessage());
        if (command.getPermissionMessage() == null)
            command.setColouredPermissionMessage(plugin.getDefaultPermissionMessage());
    }

}
