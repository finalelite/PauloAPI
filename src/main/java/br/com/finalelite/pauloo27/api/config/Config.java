package br.com.finalelite.pauloo27.api.config;

import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class Config {

    @Getter
    private String name;
    @Getter
    private FileConfiguration config;
    @Getter
    private File file;
    private Plugin plugin;

    public Config(String name, Plugin plugin) {
        this(name, plugin, new File(plugin.getDataFolder(), name));
    }

    public Config(String name, Plugin plugin, File file) {
        this.name = name;
        this.plugin = plugin;
        this.file = file;
        reloadConfig();
    }

    public void reloadConfig() {
        config = YamlConfiguration.loadConfiguration(file);
    }

    public void saveDefaultConfig(boolean override) {
        plugin.saveResource(name, override);
    }

    public void saveConfig() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
