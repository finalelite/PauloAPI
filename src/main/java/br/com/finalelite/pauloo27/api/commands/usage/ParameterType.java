package br.com.finalelite.pauloo27.api.commands.usage;

/**
 * A RSpigot class.
 * Part of the command API usage. Represents a parameter type.
 *
 * @author Paulo
 * @version 1.0
 */
public class ParameterType<T> {

    private final ParameterChecker checker;
    private final ParameterParser parser;

    /**
     * Builds a parameter type.
     *
     * @param checker A type checker
     * @param parser  A type parser
     */
    public ParameterType(ParameterChecker checker, ParameterParser parser) {
        this.checker = checker;
        this.parser = parser;
    }

    /**
     * Checks if an argument is valid for this type.
     *
     * @param argument The argument
     * @return If the argument is valid for this type.
     */
    public boolean check(String argument) {
        return checker.isValid(argument);
    }

    /**
     * Parses an argument as this type.
     *
     * @param argument The argument
     * @return The parsed argument or null if this argument isn't valid.
     */
    public T parse(String argument) {
        if (!check(argument))
            return null;

        return (T) parser.parse(argument);
    }

    @FunctionalInterface
    public interface ParameterChecker {
        boolean isValid(String argument);
    }

    @FunctionalInterface
    public interface ParameterParser {
        Object parse(String argument);
    }
}
