package br.com.finalelite.pauloo27.api.commands;

import br.com.finalelite.pauloo27.api.commands.usage.Usage;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

/**
 * A RSpigot class.
 * Part of the command API. Extends it to crate a command.
 *
 * @author Paulo
 * @version 1.0
 */
public abstract class BaseCommand extends Command {

    private String fallback;
    private String cannotRunMessage;
    protected GenericCommandSender genericListener;
    protected PlayerCommandListener playerListener;
    protected ConsoleCommandListener consoleListener;
    protected CommandBlockListener commandBlockListener;
    public Usage usage;

    /**
     * Builds a command.
     *
     * @param name The command name
     */
    protected BaseCommand(String name) {
        super(name);
        usage = new Usage(); // Empty usage
    }

    /**
     * Gets the cannot run message.
     *
     * @return The cannot run message
     * @see CommandManager#register(BaseCommand, CommandablePlugin) returns the plugin's default if its null
     */
    public String getCannotRunMessage() {
        return cannotRunMessage;
    }

    /**
     * Sets the cannot run message. The default is "&cYou cannot run this command.".
     *
     * @param message The message
     * @see #setColouredCannotRunMessage(String)
     */
    public void setCannotRunMessage(String message) {
        cannotRunMessage = message;
    }

    /**
     * Sets the cannot run message to a coloured message.
     *
     * @param replacer The char to replace colors
     * @param message  The message
     */
    public void setColouredCannotRunMessage(char replacer, String message) {
        setCannotRunMessage(ChatColor.translateAlternateColorCodes(replacer, message));
    }

    /**
     * Sets the cannot run message to a coloured message with '&' as replacer char.
     *
     * @param message The message
     */
    public void setColouredCannotRunMessage(String message) {
        setColouredCannotRunMessage('&', message);
    }

    /**
     * Sets the command fallback.
     *
     * @param fallback The fallback
     */
    public void setFallback(String fallback) {
        this.fallback = fallback;
    }

    /**
     * Gets the command fallback. The default fallback is "rspigot".
     *
     * @return The command's fallback
     */
    public String getFallback() {
        return fallback;
    }

    /**
     * Gets the usage message. Returns the {@link #usage} message or the usage of the Bukkit command api if the RSpigot
     * usage is null.
     *
     * @return The usage message
     */
    @Override
    public String getUsage() {
        return usage == null ? super.getUsage() : String.format(usage.getUsageMessageFormat(), getName());
    }

    /**
     * Executes the command.
     *
     * @param sender       Source object which is executing this command
     * @param commandLabel The alias of the command used
     * @param args         All arguments passed to the command, split via ' '
     * @return true if the command was successful, otherwise false
     */
    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        boolean b = runCommand(sender, commandLabel, args);
        if (!b)
            if (usage != null)
                sender.sendMessage(String.format(usage.getUsageMessageFormat(), commandLabel));
            else
                sender.sendMessage("Invalid usage.");
        return b;
    }

    private boolean runCommand(CommandSender sender, String commandLabel, String[] args) {
        // true if the command was successful, otherwise false
        if (getPermission() != null && !sender.hasPermission(getPermission())) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getPermissionMessage()));
            return true;
        }
        if (usage != null) {
            if (!usage.isValid(args)) {
                return false;
            }
            if (!usage.hasPermission(sender, args.length)) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getPermissionMessage()));
                return true;
            }
        }

        if (sender instanceof Player && playerListener != null) {
            return playerListener.onPlayerExecute(new ExecutedCommand<>((Player) sender, commandLabel, args, usage));
        } else if (sender instanceof ConsoleCommandSender && consoleListener != null) {
            return consoleListener.onConsoleExecute(new ExecutedCommand<>((ConsoleCommandSender) sender, commandLabel, args, usage));
        } else if (sender instanceof BlockCommandSender && commandBlockListener != null) {
            return commandBlockListener.onCommandBlockExecute(new ExecutedCommand<>((BlockCommandSender) sender, commandLabel, args, usage));
        }
        if (genericListener == null) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', cannotRunMessage));
            return true;
        }
        return genericListener.onGenericExecute(new ExecutedCommand<>(sender, commandLabel, args, usage));
    }

    /**
     * Sets the no-permission message to a coloured message.
     *
     * @param replacer          The replacer char
     * @param permissionMessage The message
     * @return
     */
    public Command setColouredPermissionMessage(char replacer, String permissionMessage) {
        return super.setPermissionMessage(ChatColor.translateAlternateColorCodes(replacer, permissionMessage));
    }

    /**
     * Sets the no-permission message to a coloured message with '&' as the replacer.
     *
     * @param permissionMessage The message
     * @return
     */
    public Command setColouredPermissionMessage(String permissionMessage) {
        return setColouredPermissionMessage('&', permissionMessage);
    }

    /**
     * Gets the tab completer list.
     *
     * @param sender   Source object which is executing this command
     * @param alias    the alias being used
     * @param args     All arguments passed to the command, split via ' '
     * @param location the location of the block the player is looking at
     * @return
     * @throws IllegalArgumentException
     */
    @Override
    public final List<String> tabComplete(CommandSender sender, String alias, String[] args, Location location) throws IllegalArgumentException {
        return onTabCompleter(sender, alias, args, location);
    }

    /**
     * Override it to set the tab completer.
     *
     * @param sender   Source object which is executing this command
     * @param alias    the alias being used
     * @param args     All arguments passed to the command, split via ' '
     * @param location the location of the block the player is looking at
     * @return
     */
    protected List<String> onTabCompleter(CommandSender sender, String alias, String[] args, Location location) {
        return super.tabComplete(sender, alias, args, location);
    }

    public interface GenericCommandSender {
        boolean onGenericExecute(ExecutedCommand<CommandSender> sender);
    }

    public interface PlayerCommandListener {
        boolean onPlayerExecute(ExecutedCommand<Player> sender);
    }

    public interface ConsoleCommandListener {
        boolean onConsoleExecute(ExecutedCommand<ConsoleCommandSender> sender);
    }

    public interface CommandBlockListener {
        boolean onCommandBlockExecute(ExecutedCommand<BlockCommandSender> sender);
    }

}
