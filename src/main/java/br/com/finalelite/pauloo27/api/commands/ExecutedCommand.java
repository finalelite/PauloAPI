package br.com.finalelite.pauloo27.api.commands;

import br.com.finalelite.pauloo27.api.commands.usage.ParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Usage;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * A RSpigot class.
 * Part of the command API. Represents an executed command.
 *
 * @author Paulo
 * @version 1.0
 */
public class ExecutedCommand<T extends CommandSender> {

    private T sender;
    private String label;
    private String[] args;
    private Usage usage;

    /**
     * Builds a new executed command.
     *
     * @param sender Source object which is executing this command
     * @param label  The alias of the command used
     * @param args   All arguments passed to the command, split via ' '
     */
    public ExecutedCommand(T sender, String label, String[] args) {
        this.sender = sender;
        this.args = args;
        this.label = label;
    }

    /**
     * Builds a new executed command.
     *
     * @param sender Source object which is executing this command
     * @param label  The alias of the command used
     * @param args   All arguments passed to the command, split via ' '
     * @param usage  The usage of the executed command
     */
    public ExecutedCommand(T sender, String label, String[] args, Usage usage) {
        this(sender, label, args);
        this.usage = usage;
    }

    /**
     * Checks if has usage.
     *
     * @return If the usage is not null
     */
    public boolean hasUsage() {
        return usage != null;
    }

    /**
     * Replies the command. Sends a coloured message to the command sender.
     *
     * @param colouredMessage The message
     */
    public void reply(String colouredMessage) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', colouredMessage));
    }

    /**
     * Checks if an argument is present.
     *
     * @param index The argument index
     * @return If arguments length is bigger than the index
     */
    public boolean hasArgument(int index) {
        return args.length > index;
    }

    /**
     * Gets the raw String of an index.
     *
     * @param index The argument index
     * @return The raw argument
     */
    public String getRawArgument(int index) {
        return args[index];
    }

    /**
     * Gets argument of the {@link ParameterType#parse(String)}.
     *
     * @param index The argument index
     * @return The argument
     */
    public Object getArgument(int index) {
        if (!hasUsage())
            return null;
        return usage.getParameters().get(index).getType().parse(args[index]);
    }

    /**
     * Gets the argument at an index as a String.
     *
     * @param index The index
     * @return The argument as String.
     */
    public String getArgumentAsString(int index) {
        return (String) getArgument(index);
    }

    /**
     * Gets the argument at an index as a char.
     *
     * @param index The index
     * @return The argument as char.
     */
    public char getArgumentAsChar(int index) {
        return (char) getArgument(index);
    }

    /**
     * Gets the argument at an index as an int.
     *
     * @param index The index
     * @return The argument as int.
     */
    public int getArgumentAsInteger(int index) {
        return (int) getArgument(index);
    }

    /**
     * Gets the argument at an index as a long.
     *
     * @param index The index
     * @return The argument as int.
     */
    public long getArgumentAsLong(int index) {
        return (long) getArgument(index);
    }

    /**
     * Gets the argument at an index as a float.
     *
     * @param index The index
     * @return The argument as float.
     */
    public float getArgumentAsFloat(int index) {
        return (float) getArgument(index);
    }

    /**
     * Gets the argument at an index as a double.
     *
     * @param index The index
     * @return The argument as double.
     */
    public double getArgumentAsDouble(int index) {
        return (double) getArgument(index);
    }

    /**
     * Gets the argument at an index as a player.
     *
     * @param index The index
     * @return The argument as player.
     */
    public Player getArgumentAsPlayer(int index) {
        return (Player) getArgument(index);
    }

    /**
     * Gets the argument at an index as a boolean.
     *
     * @param index The index
     * @return The argument as boolean.
     */
    public boolean getArgumentAsBoolean(int index) {
        return (boolean) getArgument(index);
    }

    /**
     * Gets the argument at an index as the same type of the variable declaration.
     *
     * @param index The index.
     * @param <R>   The parameter type
     * @return The argument as the type.
     */
    public <R> R getArgumentAs(int index) {
        return (R) getArgument(index);
    }

    /**
     * Gets the argument at an index as a class type.
     *
     * @param index The index.
     * @param type  The type to return
     * @param <R>   The parameter type
     * @return The argument as the type.
     */
    public <R> R getArgumentAs(int index, R type) {
        return (R) getArgument(index);
    }

    /**
     * Gets the command sender.
     *
     * @return The command sender.
     */
    public T getSender() {
        return sender;
    }

    /**
     * Gets the command label.
     *
     * @return The alias of the command used
     */
    public String getLabel() {
        return label;
    }

    /**
     * Gets the arguments array.
     *
     * @return The arguments
     */
    public String[] getArguments() {
        return args;
    }

    /**
     * Gets the arguments before and including and index.
     *
     * @param index     The starts index
     * @param delimiter The arguments delimiter
     * @return Tbe arguments joined
     */
    public String getRawArgumentsBeforeAndIncluding(int index, String delimiter) {
        return String.join(delimiter, Arrays.stream(args).skip(index).collect(Collectors.toList()));
    }

    /**
     * Gets the arguments before and including and index using " " as delimiter.
     *
     * @param index The starts index
     * @return Tbe arguments joined
     */
    public String getRawArgumentsBeforeAndIncluding(int index) {
        return getRawArgumentsBeforeAndIncluding(index, " ");
    }

}
