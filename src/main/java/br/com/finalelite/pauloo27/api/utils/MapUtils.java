package br.com.finalelite.pauloo27.api.utils;

import lombok.val;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MapUtils {

    public static <T> Map<T, Integer> sortMap(Map<T, Integer> map, int limit) {
        val sortedList = map.entrySet()
                .stream()
                .distinct()
                .limit(limit)
                .sorted(Comparator.comparingInt(o -> (int) o.getValue())).collect(Collectors.toList());
        // reverse
        Collections.reverse(sortedList);

        // create a map
        val sorted = new LinkedHashMap<T, Integer>();
        sortedList.forEach(entry -> sorted.put(entry.getKey(), entry.getValue()));
        return sorted;
    }

}
