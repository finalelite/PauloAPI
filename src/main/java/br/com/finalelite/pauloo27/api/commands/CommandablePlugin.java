package br.com.finalelite.pauloo27.api.commands;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * A RSpigot class.
 * Part of the command API. Represents a plugin that uses this Command API.
 *
 * @author Paulo
 * @version 1.0
 */
public abstract class CommandablePlugin extends JavaPlugin {
    @Getter
    protected String defaultFallback = "finalelite";
    @Getter
    protected String defaultUsageMessage = "&cUse /%%s %s.";
    @Getter
    protected String defaultCannotRunMessage = "&cVocê não pode usar esse comando.";
    @Getter
    protected String defaultPermissionMessage = "&cVocê não tem permissão.";
    @Getter
    protected CommandManager commandManager = new CommandManager();

    public void registerCommand(BaseCommand command) {
        commandManager.register(command, this);
    }

}
