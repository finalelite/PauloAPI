package br.com.finalelite.pauloo27.api.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class AccountInfo {
    private String uuid;
    private PlayerRole role;
    private PlayerGuild guild;
}
