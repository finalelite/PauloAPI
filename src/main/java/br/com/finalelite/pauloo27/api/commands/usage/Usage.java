package br.com.finalelite.pauloo27.api.commands.usage;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

/**
 * A RSpigot class.
 * Part of the command API usage. Represents a command usage.
 *
 * @author Paulo
 * @version 1.0
 */
public class Usage {

    private List<Parameter> parameters = new ArrayList<>();
    private String usageMessageFormat;

    /**
     * Builds an empty usage.
     */
    public Usage() {
    }

    /**
     * Builds an empty usage and sets the usage message format. The default is "&cUse /%%s %s.".
     *
     * @param usageMessageFormat The usage message format
     */
    public Usage(String usageMessageFormat) {
        this.usageMessageFormat = usageMessageFormat;
    }

    /**
     * Adds a parameter to the usage.
     *
     * @param parameter The parameter
     * @return This usage instance
     */
    public Usage addParameter(Parameter parameter) {
        parameters.add(parameter);

        return this;
    }

    /**
     * Sets the usage message format. The default value is "&cUse /%%s %s.". The first replacer is the command label and
     * the second is the parameters.
     *
     * @param colouredMessage The message format
     */
    public void setUsageMessageFormat(String colouredMessage) {
        this.usageMessageFormat = colouredMessage;
    }

    /**
     * The coloured usage message format.
     *
     * @return The coloured usage format, using the default "&cUse /%%s %s." if null.
     */
    public String getUsageMessageFormat() {
        String format;
        if (usageMessageFormat == null) {
            format = "&cUse /%%s %s.";
        } else {
            format = usageMessageFormat;
        }
        StringBuilder sb = new StringBuilder();
        parameters.forEach(parameter -> {
            if (parameter.isRequired()) {
                sb.append("<");
                sb.append(parameter.getName());
                sb.append(">");
            } else {
                sb.append("[");
                sb.append(parameter.getName());
                sb.append("]");
            }
            sb.append(" ");
        });
        return String.format(ChatColor.translateAlternateColorCodes('&', format), sb.toString().trim());
    }

    /**
     * Gets a copy of the parameters list.
     *
     * @return A copy of the parameters list.
     */
    public List<Parameter> getParameters() {
        return new ArrayList<>(parameters);
    }

    /**
     * Checks if an arguments array are valid.
     *
     * @param args The arguments
     * @return If the arguments are valid
     */
    public boolean isValid(String[] args) {
        if (parameters.size() == 0) return true; // if hasn't parameters return true
        for (int i = 0; i < parameters.size(); i++) {
            if (args != null && args.length > i) { // check if the argument exists
                if (!parameters.get(i).getType().check(args[i])) return false; // if ins't valid return false
            } else if (parameters.get(i).isRequired()) return false; // if not exists return false
        }
        return true; // if passed in everything return true;
    }

    /**
     * Checks if the sender has permission to run the command with the parameters count.
     *
     * @param sender The sender
     * @param count  The arguments size
     * @return If the sender has permission
     */
    public boolean hasPermission(CommandSender sender, int count) {
        if (parameters.size() == 0) return true; // if hasn't parameters return true
        for (int i = 0; i < count; i++) {
            if (i == parameters.size())
                return true;
            if (parameters.get(i).getPermission() != null && !sender.hasPermission(parameters.get(i).getPermission()))
                return false; // check if the param as custom perm and then check it
        }
        return true; // if passed in everything return true;
    }

}
