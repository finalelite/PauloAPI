package br.com.finalelite.pauloo27.api.database;

import lombok.Data;

@Data
public class DatabaseInformation {

    private final String address;
    private final int port;
    private final String username;
    private final String password;

}
