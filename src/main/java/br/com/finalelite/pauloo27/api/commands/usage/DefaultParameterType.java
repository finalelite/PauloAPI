package br.com.finalelite.pauloo27.api.commands.usage;

import org.bukkit.Bukkit;

/**
 * A RSpigot class.
 * Part of the command API usage. A enum of defaults parameter type.
 *
 * @author Paulo
 * @version 1.0
 */
public enum DefaultParameterType {
    /**
     * A String.
     */
    STRING(new ParameterType(argument -> true, argument -> argument)),
    /**
     * A char.
     */
    CHAR(new ParameterType(argument -> argument.length() == 1, argument -> argument.charAt(0))),
    /**
     * An integer number.
     */
    INTEGER(new ParameterType(argument -> {
        try {
            Integer.parseInt(argument);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }, Integer::parseInt)),
    /**
     * A long number.
     */
    LONG(new ParameterType(argument -> {
        try {
            Long.parseLong(argument);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }, Long::parseLong)),
    /**
     * A float number.
     */
    FLOAT(new ParameterType(argument -> {
        try {
            Float.parseFloat(argument);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }, Float::parseFloat)),
    /**
     * A double number.
     */
    DOUBLE(new ParameterType(argument -> {
        try {
            Double.parseDouble(argument);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }, Double::parseDouble)),
    /**
     * An online player.
     */
    PLAYER(new ParameterType(argument -> Bukkit.getPlayer(argument) != null, Bukkit::getPlayer)),
    /**
     * A boolean.
     */
    BOOLEAN(new ParameterType(argument ->
            argument.equalsIgnoreCase("true") || argument.equalsIgnoreCase("false"),
            Boolean::parseBoolean));

    final ParameterType type;

    DefaultParameterType(ParameterType type) {
        this.type = type;
    }

    public ParameterType getType() {
        return type;
    }
}
