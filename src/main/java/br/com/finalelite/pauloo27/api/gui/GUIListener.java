package br.com.finalelite.pauloo27.api.gui;

import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

@AllArgsConstructor
public class GUIListener implements Listener {

    private GUIManager guiManager;

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player))
            return;

        val player = (Player) event.getWhoClicked();
        if (!guiManager.hasOpenedGUI(player))
            return;

        val gui = guiManager.getOpenedGUI(player);

        if (!gui.getInventory(player).equals(player.getOpenInventory().getTopInventory())) {
            player.sendMessage(ChatColor.RED + "Houston, temos um problema.");
            player.closeInventory();
            event.setCancelled(true);
            guiManager.onClose(player);
        }
        
        if (!gui.hasListener(event.getCurrentItem())) {
            event.setCancelled(true);
            return;
        }

        if(event.getCurrentItem() != null && gui.getListener(event.getCurrentItem()) != null)
            gui.getListener(event.getCurrentItem()).onClick(event);
        
        event.setCancelled(true);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (!(event.getPlayer() instanceof Player))
            return;

        val player = (Player) event.getPlayer();
        if (!guiManager.hasOpenedGUI(player))
            return;

        guiManager.onClose(player);
    }

}
