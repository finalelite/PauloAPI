package br.com.finalelite.pauloo27.api.database;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
public enum PlayerRole {
    // STAFFERS
    GAME_MASTER("Master", "Master", "&6"),
    SUPERVISOR("Supervisor", "Supervisor", "&4"),
    ADMIN("Admin", "Admin", "&c"),
    MODERADOR("Moderador", "Moderador", "&2"),
    SUPORTE("Suporte", "Suporte", "&a"),

    // SPECIAL
    YOUTUBER("Youtuber", "YouTuber", "&c"),

    // VIPS
    TITAN("Vip+++", "Titan", "&5"),
    DUQUE("Vip++", "Duque", "&4"),
    LORD("Vip+", "Lord", "&b"),
    CONDE("Vip", "Conde", "&e"),

    // DEFAULT
    MEMBRO("Membro", "Membro", "&7");

    @Getter
    private final String value;
    @Getter
    private final String displayName;
    @Getter
    private final String color;

    @Getter
    private static List<PlayerRole> staffersRole = Arrays.asList(GAME_MASTER, SUPERVISOR, ADMIN, MODERADOR, SUPORTE);
    @Getter
    private static List<PlayerRole> majorStaffersRole = Arrays.asList(GAME_MASTER, SUPERVISOR, ADMIN);


    public static PlayerRole fromRealName(String string) {
        if (string == null)
            return null;
        return Arrays.stream(PlayerRole.values()).filter(role -> role.getValue().equalsIgnoreCase(string)).findFirst().orElse(null);
    }

    public static PlayerRole fromName(String string) {
        if (string == null)
            return null;
        return Arrays.stream(PlayerRole.values()).filter(role -> role.name().equalsIgnoreCase(string)).findFirst().orElse(null);
    }

    public boolean isStaff() {
        return staffersRole.contains(this);
    }

    public boolean isMajorStaff() {
        return majorStaffersRole.contains(this);
    }

    public boolean isVIP() {
        return this.getValue().startsWith("Vip");
    }

}
