package br.com.finalelite.pauloo27.api;

import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import br.com.finalelite.pauloo27.api.database.AccountInfo;
import br.com.finalelite.pauloo27.api.database.AccountsCache;
import br.com.finalelite.pauloo27.api.database.DatabaseInformation;
import br.com.finalelite.pauloo27.api.database.EconomyManager;
import br.com.finalelite.pauloo27.api.gui.GUIManager;
import br.com.finalelite.pauloo27.api.listeners.PlayerListeners;
import br.com.finalelite.pauloo27.api.message.Locale;
import com.gitlab.pauloo27.core.sql.EzSQL;
import com.gitlab.pauloo27.core.sql.EzSQLType;
import lombok.Getter;
import lombok.val;
import org.bukkit.entity.Player;

import java.io.File;
import java.sql.SQLException;

public class PauloAPI extends CommandablePlugin {

    @Getter
    private static PauloAPI instance;
    @Getter
    private DatabaseInformation databaseInformation;
    @Getter
    private AccountsCache accountsCache;
    @Getter
    private EzSQL sql;
    @Getter
    private GUIManager guiManager;
    @Getter
    private Locale portugueseLocale;
    @Getter
    private EconomyManager economyManager;

    @Override
    public void onEnable() {
        instance = this;
        guiManager = new GUIManager(this);


        if (!new File(getDataFolder(), "config.yml").exists())
            saveResource("config.yml", true);

        val address = getConfig().getString("DB.Address");
        val port = getConfig().getInt("DB.Port");
        val username = getConfig().getString("DB.Username");
        val password = getConfig().getString("DB.Password");

        portugueseLocale = new Locale("pt_br");

        databaseInformation = new DatabaseInformation(address, port, username, password);

        try {
            sql = getEzSQL("mixedup").connect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        accountsCache = new AccountsCache(sql);
        economyManager = new EconomyManager(sql);

        getServer().getPluginManager().registerEvents(new PlayerListeners(), this);
    }

    public EzSQL getEzSQL(String database) {
        return new EzSQL(EzSQLType.MYSQL)
                .withDefaultDatabase(database)
                .withAddress(databaseInformation.getAddress(), databaseInformation.getPort())
                .withLogin(databaseInformation.getUsername(), databaseInformation.getPassword())
                .withCustomDriver("com.mysql.jdbc.Driver");
    }

    public EzSQL getEzSQL() {
        return new EzSQL(EzSQLType.MYSQL)
                .withAddress(databaseInformation.getAddress(), databaseInformation.getPort())
                .withLogin(databaseInformation.getUsername(), databaseInformation.getPassword())
                .withCustomDriver("com.mysql.jdbc.Driver");
    }

    public AccountInfo getAccountInfo(Player player) {
        return accountsCache.getAccountInfo(player);
    }

}
