package br.com.finalelite.pauloo27.api.utils;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class InventoryUtils {

    public static boolean hasSpace(Inventory inventory, Material material, int amount) {
        if (amount <= 0)
            return true;

        int emptySlots = (int) Arrays.stream(inventory.getStorageContents()).filter(Objects::isNull).count();
        int minSlots = Math.max(1, (int) Math.ceil(((double) amount) / material.getMaxStackSize()));

        if (emptySlots >= minSlots)
            return true;

        AtomicInteger space = new AtomicInteger();

        space.getAndAdd(emptySlots * 64);

        inventory.all(material).forEach((slot, stack) -> {
            if (stack.getMaxStackSize() > stack.getAmount()) {
                space.getAndAdd(stack.getMaxStackSize() - stack.getAmount());
            }
        });

        return amount <= space.get();
    }

}
