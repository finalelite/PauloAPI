package br.com.finalelite.pauloo27.api.gui;

import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class BaseGUI {

    private Map<ItemStack, GUIClick> listeners = new HashMap<>();
    private Map<Player, Inventory> inventories = new HashMap<>();

    public Inventory open(Player player) {
        val inventory = buildInventory(player);
        setInventory(player, inventory);
        return inventory;
    }

    protected void setInventory(Player player, Inventory inventory) {
        inventories.put(player, inventory);
    }

    public Inventory getInventory(Player player) {
        return inventories.get(player);
    }

    public abstract Inventory buildInventory(Player player);

    protected void addListener(ItemStack item, GUIClick action) {
        listeners.put(item, action);
    }

    public boolean hasListener(ItemStack item) {
        return listeners.containsKey(item);
    }

    public GUIClick getListener(ItemStack item) {
        return listeners.get(item);
    }

    public ItemStack addItem(Inventory inv, Material material, int amount, String name, List<String> lore, GUIClick action) {
        return addItem(inv, buildItem(material, amount, name, lore), action);
    }

    public ItemStack addItem(Inventory inv, ItemStack itemStack, GUIClick action) {
        inv.addItem(itemStack);
        addListener(itemStack, action);
        return itemStack;
    }

    public ItemStack setItem(Inventory inv, int slot, Material material, int amount, String name, List<String> lore, GUIClick action) {
        return setItem(inv, slot, buildItem(material, amount, name, lore), action);
    }
    
    public ItemStack setItem(Inventory inv, int slot, ItemStack itemStack, GUIClick action) {
        inv.setItem(slot, itemStack);
        addListener(itemStack, action);
        return itemStack;
    }

    public ItemStack buildItem(Material material, int amount, String name, List<String> lore) {
        val itemStack = new ItemStack(material, amount);
        val itemMeta = itemStack.getItemMeta();

        if (name != null)
            itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        if (lore != null)
            itemMeta.setLore(lore.stream().map(line -> ChatColor.translateAlternateColorCodes('&', line))
                    .collect(Collectors.toList()));
        else
            itemMeta.setLore(null);

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

}
