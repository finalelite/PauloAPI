package br.com.finalelite.pauloo27.api.config;

import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class ConfigUtils {

    public static String serializeLocation(Location location) {
        return location.getWorld().getName() + "," + location.getX() + "," + location.getY() + "," + location.getZ() + "," + location.getYaw() + "," + location.getPitch();
    }

    public static Location deserializeLocation(String location) {
        val loc = location.split("\\,");
        val w = Bukkit.getWorld(loc[0]);
        val x = Double.parseDouble(loc[1]);
        val y = Double.parseDouble(loc[2]);
        val z = Double.parseDouble(loc[3]);
        val yaw = Float.parseFloat(loc[4]);
        val pitch = Float.parseFloat(loc[5]);
        return new Location(w, x, y, z, yaw, pitch);
    }

}
