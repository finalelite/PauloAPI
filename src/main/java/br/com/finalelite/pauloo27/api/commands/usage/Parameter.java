package br.com.finalelite.pauloo27.api.commands.usage;

/**
 * A RSpigot class.
 * Part of the command API usage. Represents a parameter.
 *
 * @author Paulo
 * @version 1.0
 */
public class Parameter {

    private String name;
    private ParameterType type;
    private boolean required;
    private String permission;

    /**
     * Builds a new parameter.
     *
     * @param name     The parameter name
     * @param type     The parameter type
     * @param required If the parameter is required
     */
    public Parameter(String name, ParameterType type, boolean required) {
        this.name = name;
        this.type = type;
        this.required = required;
    }

    /**
     * Builds a new parameter.
     *
     * @param name       The parameter name
     * @param type       The parameter type
     * @param required   If the parameter is required
     * @param permission The permission to run the command with this parameter
     */
    public Parameter(String name, ParameterType type, boolean required, String permission) {
        this.name = name;
        this.type = type;
        this.required = required;
        this.permission = permission;
    }

    /**
     * Builds a new parameter.
     *
     * @param name     The parameter name
     * @param type     The parameter type
     * @param required If the parameter is required
     */
    public Parameter(String name, DefaultParameterType type, boolean required) {
        this.name = name;
        this.type = type.getType();
        this.required = required;
    }

    /**
     * Builds a new parameter.
     *
     * @param name       The parameter name
     * @param type       The parameter type
     * @param required   If the parameter is required
     * @param permission The permission to run the command with this parameter
     */
    public Parameter(String name, DefaultParameterType type, boolean required, String permission) {
        this.name = name;
        this.type = type.getType();
        this.required = required;
        this.permission = permission;
    }

    /**
     * Gets the parameter name.
     *
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the parameter permission.
     *
     * @return The permission
     */
    public String getPermission() {
        return permission;
    }

    /**
     * Gets the parameter type.
     *
     * @return The type
     */
    public ParameterType getType() {
        return type;
    }

    /**
     * Checks if the command is required.
     *
     * @return If is required
     */
    public boolean isRequired() {
        return required;
    }

}
