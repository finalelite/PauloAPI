package br.com.finalelite.pauloo27.api.listeners;

import br.com.finalelite.pauloo27.api.PauloAPI;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListeners implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent event) {
        PauloAPI.getInstance().getAccountsCache().removeFromCache(event.getPlayer());
    }
}
