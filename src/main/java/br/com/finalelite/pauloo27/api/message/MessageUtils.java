package br.com.finalelite.pauloo27.api.message;

import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Locale;

public class MessageUtils {

    private static final String alphanumericChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final DecimalFormat decimalFormatter;
    private static final DecimalFormat realFormatter;

    static {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');
        decimalFormatter = new DecimalFormat("#,##0.00", otherSymbols);
        realFormatter = new DecimalFormat("#,###", otherSymbols);
    }

    public static String capitalize(String message) {
        return Character.toUpperCase(message.charAt(0)) + message.substring(1);
    }

    public static String colourMessage(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static void sendColouredMessage(CommandSender sender, String message) {
        Arrays.stream(message.split("\n")).forEach(msg -> sender.sendMessage(colourMessage(msg)));
    }

    public static void sendColouredMessage(CommandSender sender, String... messages) {
        Arrays.stream(messages).forEach(msg -> sender.sendMessage(colourMessage(msg)));
    }

    public static String pluralize(int size, String singular, String plural) {
        return size + " " + (size == 1 ? singular : plural);
    }

    public static String formatDecimalToEuropeanLocale(double value) {
        return decimalFormatter.format(value);
    }

    public static String formatRealToEuropeanLocale(long value) {
        return realFormatter.format(value);
    }

    public static String formatRealToEuropeanLocale(int value) {
        return realFormatter.format(value);
    }

    public static String getRandomAlphanumericString(int len) {
        val sb = new StringBuilder(len);
        val random = new SecureRandom();
        for (int i = 0; i < len; i++)
            sb.append(alphanumericChars.charAt(random.nextInt(alphanumericChars.length())));
        return sb.toString();
    }

}
