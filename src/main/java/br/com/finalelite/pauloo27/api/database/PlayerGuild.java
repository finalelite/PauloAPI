package br.com.finalelite.pauloo27.api.database;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;

import java.util.Arrays;

@RequiredArgsConstructor
public enum PlayerGuild {
    SANGUINARIA_1("sanguinaria", 1, "Sanguinária"),
    SANGUINARIA_2("sanguinaria", 2, "Sanguinária"),
    SANGUINARIA_3("sanguinaria", 3, "Sanguinária"),
    SANGUINARIA_4("sanguinaria", 4, "Sanguinária"),
    SANGUINARIA_5("sanguinaria", 5, "Sanguinária"),
    ANCIA_1("ancia", 1, "Anciã"),
    ANCIA_2("ancia", 2, "Anciã"),
    ANCIA_3("ancia", 3, "Anciã"),
    ANCIA_4("ancia", 4, "Anciã"),
    ANCIA_5("ancia", 5, "Anciã"),
    NOBRE_1("nobre", 1, "Nobre"),
    NOBRE_2("nobre", 2, "Nobre"),
    NOBRE_3("nobre", 3, "Nobre"),
    NOBRE_4("nobre", 4, "Nobre"),
    NOBRE_5("nobre", 5, "Nobre");

    @Getter
    private final String name;
    @Getter
    private final int level;
    @Getter
    private final String displayName;


    public static PlayerGuild fromValue(String string) {
        if (string == null)
            return null;
        return Arrays.stream(PlayerGuild.values())
                .filter(
                        guild -> guild.getName().equalsIgnoreCase(string.split("_")[0]) &&
                                guild.getLevel() == Integer.parseInt(string.split("_")[1]))
                .findFirst().orElse(null);
    }

    public static PlayerGuild fromNameAndLevel(String guildName, int level) {
        if (guildName == null || level == -1)
            return null;

        val string = guildName.toLowerCase() + "_" + level;
        return Arrays.stream(PlayerGuild.values()).filter(guild -> guild.name().equalsIgnoreCase(string)).findFirst().orElse(null);
    }

    public boolean isNobre() {
        return this.name.equalsIgnoreCase("nobre");
    }

    public boolean isAncia() {
        return this.name.equalsIgnoreCase("ancia");
    }

    public boolean isSanguinaria() {
        return this.name.equalsIgnoreCase("sanguinaria");
    }
}

