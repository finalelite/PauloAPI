package br.com.finalelite.pauloo27.api.database;

import com.gitlab.pauloo27.core.sql.EzSQL;
import com.gitlab.pauloo27.core.sql.EzSelect;
import com.gitlab.pauloo27.core.sql.EzTable;
import com.gitlab.pauloo27.core.sql.EzUpdate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.val;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class EconomyManager {

    private LoadingCache<String, PlayerEconomy> cache;
    private EzTable coinsTable;
    private List<EconomyListener> listeners = new ArrayList<>();


    public EconomyManager(EzSQL sql) {
        coinsTable = sql.getTable("mixedup.Coins_data");
        clearCache();
    }

    public void clearCache() {
        cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterAccess(10, TimeUnit.MINUTES)
                .build(new CacheLoader<String, PlayerEconomy>() {
                    public PlayerEconomy load(String uuid) {
                        return loadEconomyFromDB(uuid);
                    }
                });
    }

    public void addListener(EconomyListener listener) {
        listeners.add(listener);
    }

    public void removeFromCache(String uuid) {
        cache.invalidate(uuid);
    }

    public void removeFromCache(Player player) {
        removeFromCache(player.getUniqueId().toString());
    }

    private PlayerEconomy loadEconomyFromDB(String uuid) {
        try (val rs = coinsTable.select(new EzSelect("coins, blackcoins")
                .where().equals("UUID", uuid)).getResultSet()) {
            if (rs.next())
                return new PlayerEconomy(rs.getDouble("coins"), rs.getDouble("blackcoins"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PlayerEconomy getPlayerEconomy(Player player) {
        return getPlayerEconomy(player.getUniqueId().toString());
    }

    public PlayerEconomy getPlayerEconomy(String uuid) {
        try {
            return cache.get(uuid);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean deposit(Player player, PlayerCoinType type, double amount) {
        return deposit(player.getUniqueId().toString(), type, amount);
    }

    public boolean deposit(String uuid, PlayerCoinType type, double amount) {
        checkParameters(type, amount);

        val economy = getPlayerEconomy(uuid);
        PlayerEconomy newEconomy;
        if (type == PlayerCoinType.COIN)
            newEconomy = new PlayerEconomy(economy.getCoins() + amount, economy.getBlackCoins());
        else
            newEconomy = new PlayerEconomy(economy.getCoins(), economy.getBlackCoins() + amount);

        val result = updateCache(uuid, type, newEconomy);
        listeners.forEach(listener -> listener.listen(uuid, type, economy.getBalance(type), newEconomy.getBalance(type)));
        return result;
    }

    public boolean withdraw(Player player, PlayerCoinType type, double amount) {
        return withdraw(player.getUniqueId().toString(), type, amount);
    }

    public boolean withdraw(String uuid, PlayerCoinType type, double amount) {
        checkParameters(type, amount);

        val economy = getPlayerEconomy(uuid);

        if (!economy.hasBalance(type, amount))
            return false;

        PlayerEconomy newEconomy;
        if (type == PlayerCoinType.COIN)
            newEconomy = new PlayerEconomy(economy.getCoins() - amount, economy.getBlackCoins());
        else
            newEconomy = new PlayerEconomy(economy.getCoins(), economy.getBlackCoins() + amount);

        val result = updateCache(uuid, type, newEconomy);
        listeners.forEach(listener -> listener.listen(uuid, type, economy.getBalance(type), newEconomy.getBalance(type)));
        return result;
    }

    public boolean setCoin(Player player, PlayerCoinType type, double amount) {
        return setCoin(player.getUniqueId().toString(), type, amount);
    }

    public boolean setCoin(String uuid, PlayerCoinType type, double amount) {
        checkParameters(type, amount);

        val economy = getPlayerEconomy(uuid);

        PlayerEconomy newEconomy;
        if (type == PlayerCoinType.COIN)
            newEconomy = new PlayerEconomy(amount, economy.getBlackCoins());
        else
            newEconomy = new PlayerEconomy(economy.getCoins(), amount);

        val result = updateCache(uuid, type, newEconomy);
        listeners.forEach(listener -> listener.listen(uuid, type, economy.getBalance(type), newEconomy.getBalance(type)));
        return result;
    }

    public boolean updateCache(Player player, PlayerCoinType type, PlayerEconomy economy) {
        return updateCache(player.getUniqueId().toString(), type, economy);
    }

    public boolean updateCache(String uuid, PlayerCoinType type, PlayerEconomy economy) {
        try {
            coinsTable.update(new EzUpdate().set(type == PlayerCoinType.COIN ? "coins" : "blackcoins", economy.getBalance(type))
                    .where().equals("uuid", uuid)).close();

            cache.put(uuid, economy);
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    private static boolean checkParameters(PlayerCoinType type, double amount) {
        return type != null && amount > 0;
    }

    @FunctionalInterface
    public interface EconomyListener {
        void listen(String uuid, PlayerCoinType type, double oldValue, double newValue);
    }

}
