package br.com.finalelite.pauloo27.api.database;

import com.gitlab.pauloo27.core.sql.EzSQL;
import com.gitlab.pauloo27.core.sql.EzSelect;
import com.gitlab.pauloo27.core.sql.EzTable;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.Getter;
import lombok.val;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class AccountsCache {

    private LoadingCache<String, AccountInfo> cache;
    @Getter
    private EzTable tagTable;
    @Getter
    private EzTable guildTable;

    public AccountsCache(EzSQL sql) {
        tagTable = sql.getTable("hubfinal.Tag_data");
        guildTable = sql.getTable("mixedup.Guilda_data");

        clearCache();
    }

    public void clearCache() {
        cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterAccess(10, TimeUnit.MINUTES)
                .build(new CacheLoader<String, AccountInfo>() {
                    public AccountInfo load(String uuid) {
                        return loadAccountFromDB(uuid);
                    }
                });
    }

    public void removeFromCache(String uuid) {
        cache.invalidate(uuid);
    }

    public void removeFromCache(Player player) {
        removeFromCache(player.getUniqueId().toString());
    }

    public AccountInfo getAccountInfo(String uuid) {
        try {
            return cache.get(uuid);
        } catch (Exception e) {
            return null;
        }
    }

    public AccountInfo getAccountInfo(Player player) {
        return getAccountInfo(player.getUniqueId().toString());
    }

    private AccountInfo loadAccountFromDB(String uuid) {
        PlayerRole role = null;
        try (val rs = tagTable.select(new EzSelect("Tag")
                .where().equals("UUID", uuid)).getResultSet()) {
            if (rs.next())
                role = PlayerRole.fromRealName(rs.getString("Tag"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        PlayerGuild guild = null;

        try (val rs = guildTable.select(new EzSelect("Guilda, Rank")
                .where().equals("UUID", uuid)).getResultSet()) {
            if (rs.next())
                guild = PlayerGuild.fromNameAndLevel(rs.getString("Guilda"), rs.getInt("Rank"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new AccountInfo(uuid, role, guild);
    }
}
