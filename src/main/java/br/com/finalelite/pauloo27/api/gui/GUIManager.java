package br.com.finalelite.pauloo27.api.gui;

import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public class GUIManager {

    private Map<Player, BaseGUI> openedGUI = new HashMap<>();
    private Map<Player, Integer> lastOpen = new HashMap<>();
    private JavaPlugin plugin;
    private List<Player> updating = new ArrayList<>();

    public GUIManager(JavaPlugin plugin) {
        Bukkit.getServer().getPluginManager().registerEvents(new GUIListener(this), plugin);
        this.plugin = plugin;
    }

    public void open(Player player, BaseGUI gui) {
        if (lastOpen.containsKey(player)) {
            val last = lastOpen.get(player);
            if ((new Date().getTime() / 1000) - last <= 5) {
                player.sendMessage(ChatColor.RED + "Aguarde...");
                player.closeInventory();
                return;
            }
        }

        lastOpen.put(player, (int) (new Date().getTime() / 1000));

        openedGUI.put(player, gui);
        player.openInventory(gui.open(player));
        updating.remove(player);
    }

    public void safeOpen(Player player, BaseGUI gui) {
        lastOpen.put(player, (int) (new Date().getTime() / 1000));

        openedGUI.put(player, gui);
        player.openInventory(gui.open(player));
        updating.remove(player);
    }

    public void close(Player player) {
        if (!openedGUI.containsKey(player))
            return;
        player.closeInventory();
        onClose(player);
    }

    public void update(Player player, BaseGUI newGUI) {
        updating.add(player);
        new BukkitRunnable() {
            @Override
            public void run() {
                safeOpen(player, newGUI);
            }
        }.runTaskLater(plugin, 5L);
    }

    public void onClose(Player player) {
        if (updating.contains(player))
            return;
        if (!openedGUI.containsKey(player))
            return;
        openedGUI.remove(player);
        BukkitTask task = new BukkitRunnable() {
            @Override
            public void run() {
                player.updateInventory();
            }
        }.runTaskLaterAsynchronously(plugin, 5);
    }

    public BaseGUI getOpenedGUI(Player player) {
        return openedGUI.get(player);
    }

    public boolean hasOpenedGUI(Player player) {
        return openedGUI.containsKey(player);
    }
}
