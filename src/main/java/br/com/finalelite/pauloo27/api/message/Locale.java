package br.com.finalelite.pauloo27.api.message;

import br.com.finalelite.pauloo27.api.PauloAPI;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Material;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.HashMap;

public class Locale {

    private final String localeName;
    private JsonObject json;
    private Map<String, String> texts = new HashMap<>();

    public Locale(String localeName) {
        this.localeName = localeName.toLowerCase();
        val folder = new File(PauloAPI.getInstance().getDataFolder(), "langs");
        if (!folder.exists())
            folder.mkdir();
        val file = new File(folder, this.localeName + ".json");

        try {
            val parser = new JsonParser();
            json = parser.parse(new FileReader(file)).getAsJsonObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getText(String key) {
        if (texts.containsKey(key))
            return texts.get(key);

        return getTextFromFile(key, null);
    }

    public String getText(String key, String defaultText) {
        if (texts.containsKey(key))
            return texts.get(key);

        texts.put(key, getTextFromFile(key, defaultText));
        return getText(key, defaultText);
    }

    public String getMaterialName(Material material) {
        if (material.isBlock())
            return getText("block.minecraft." + material.getKey().getKey(), material.name());
        else if (material.isItem())
            return getText("item.minecraft." + material.getKey().getKey(), material.name());
        else
            return material.name();
    }

    private String getTextFromFile(String key, String defaultText) {
        return json.has(key) ? json.get(key).getAsString() : defaultText;
    }


}
