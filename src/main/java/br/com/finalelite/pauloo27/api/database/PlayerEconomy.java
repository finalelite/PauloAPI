package br.com.finalelite.pauloo27.api.database;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class PlayerEconomy {

    private final double coins;
    private final double blackCoins;

    public double getBalance(PlayerCoinType type) {
        if (type == PlayerCoinType.COIN)
            return coins;
        if (type == PlayerCoinType.BLACKCOIN)
            return blackCoins;
        return -1;
    }

    public boolean hasBalance(PlayerCoinType type, double amount) {
        return getBalance(type) >= amount;
    }

}
