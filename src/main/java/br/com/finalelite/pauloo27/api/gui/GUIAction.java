package br.com.finalelite.pauloo27.api.gui;

import br.com.finalelite.pauloo27.api.PauloAPI;
import org.bukkit.entity.Player;

public class GUIAction {

    public static GUIClick updateInventory(BaseGUI newGui) {
        return event -> PauloAPI.getInstance().getGuiManager().update((Player) event.getWhoClicked(), newGui);
    }
}
