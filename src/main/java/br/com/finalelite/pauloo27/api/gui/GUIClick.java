package br.com.finalelite.pauloo27.api.gui;

import org.bukkit.event.inventory.InventoryClickEvent;

@FunctionalInterface
public interface GUIClick {
    public void onClick(InventoryClickEvent event);
}
