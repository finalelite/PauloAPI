package br.com.finalelite.pauloo27.api.scoreboard;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

@RequiredArgsConstructor
public class ScoreboardManager {

    @Getter
    private final ScoreboardBuilder scoreboard;
    @Getter
    private final TabListManager tablistManager;

    public void setScoreboard(Player player) {
        val scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        player.setScoreboard(scoreboard);

        this.scoreboard.setScoreboard(player);
        this.tablistManager.setTabListToPlayer(player);
    }

    public void updateTablist(Player player) {
        Bukkit.getOnlinePlayers().forEach(target -> tablistManager.updatePlayerGroup(player, target));
    }


}
