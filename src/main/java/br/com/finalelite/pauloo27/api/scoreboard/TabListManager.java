package br.com.finalelite.pauloo27.api.scoreboard;

import lombok.Data;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class TabListManager {

    private List<TabGroup> groups = new ArrayList<>();
    private Map<Player, TabGroup> playerGroups = new LinkedHashMap<>();
    private final NumberFormat numberFormat = new DecimalFormat("00000");

    public void addGroup(TabGroup group) {
        groups.add(group);
    }

    public int getGroupId(TabGroup group) {
        if (group == null)
            return -1;
        return groups.indexOf(group);
    }

    public int getGroupIdByName(String name) {
        return getGroupId(groups.stream().filter(group -> group.getName().equals(name)).findFirst().orElseGet(null));
    }

    public TabGroup getGroupById(int id) {
        return groups.get(id);
    }

    public void setPlayerGroup(Player player, int groupId) {
        playerGroups.put(player, getGroupById(groupId));
    }

    public void setTabListToPlayer(Player player) {
        val scoreboard = player.getScoreboard();
        IntStream.range(0, groups.size()).forEach(index -> {
            val group = groups.get(index);

            val team = scoreboard.registerNewTeam(numberFormat.format(index) + group.getName().toLowerCase());
            
            if(group.getPrefix() != null)
                team.setPrefix(ChatColor.translateAlternateColorCodes('&', group.getPrefix()));
            if(group.getSuffix() != null)
                team.setSuffix(ChatColor.translateAlternateColorCodes('&', group.getSuffix()));
            if(group.getColor() != null)
                team.setColor(group.getColor());

            playerGroups.entrySet().stream()
                    .filter(entry -> entry.getValue() == group)
                    .forEach(entry -> team.addEntry(entry.getKey().getName()));
        });
    }

    public void updatePlayerGroup(Player player, Player target) {
        val scoreboard = target.getScoreboard();
        val group = playerGroups.get(player);
        val index = groups.indexOf(group);

        val team = scoreboard.getTeam(numberFormat.format(index) + group.getName().toLowerCase());
        team.addEntry(player.getName());
    }

    @Data
    public static class TabGroup {
        private final ChatColor color;
        private final String prefix;
        private final String name;
        private final String suffix;
    }
}
