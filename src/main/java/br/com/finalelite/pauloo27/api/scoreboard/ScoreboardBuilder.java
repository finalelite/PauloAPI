package br.com.finalelite.pauloo27.api.scoreboard;

import com.google.common.base.Preconditions;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

@RequiredArgsConstructor
public class ScoreboardBuilder {

    private final String name;
    private final String title;
    private List<ScoreboardEntry> entries = new ArrayList<>();
    private List<Player> ignoreScoreboard = new ArrayList<>();
    private int lastWhiteSpace = 0;

    public void addWhiteSpace() {
        val index = lastWhiteSpace++;
        val values = ChatColor.values();
        val times = Math.max(1, index / values.length);
        val sb = new StringBuilder();
        IntStream.range(0, times).forEach(i -> sb.append(" "));
        sb.append(values[index]);

        addEntry(new ScoreboardEntry("spacer" + (lastWhiteSpace - 1), sb.toString(), player -> ""));
    }

    public boolean toggleScoreboardToPlayer(Player player) {
        if (isScoreboardEnabled(player)) {
            disableScoreboardToPlayer(player);
            return false;
        } else {
            enableScoreboardToPlayer(player);
            return true;
        }
    }

    public void disableScoreboardToPlayer(Player player) {
        if (ignoreScoreboard.contains(player))
            return;

        ignoreScoreboard.add(player);
        val scoreboard = player.getScoreboard();
        if (scoreboard == null)
            return;

        val objective = scoreboard.getObjective(name);
        if (objective == null)
            return;

        objective.unregister();
    }

    public void enableScoreboardToPlayer(Player player) {
        if (!ignoreScoreboard.contains(player))
            return;

        ignoreScoreboard.remove(player);
        setScoreboard(player);
    }

    public boolean isScoreboardEnabled(Player player) {
        return !ignoreScoreboard.contains(player);
    }

    public void addEntry(ScoreboardEntry entry) {
        Preconditions.checkArgument(entry.getName().length() <= 32);
        entries.add(entry);
    }

    public void setScoreboard(Player player) {
        if (ignoreScoreboard.contains(player))
            return;
        val scoreboard = player.getScoreboard();
        val objective = scoreboard.getObjective(name) == null ? scoreboard.registerNewObjective(name, "stats", ChatColor.translateAlternateColorCodes('&', title)) : scoreboard.getObjective(name);
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        val size = entries.size();
        IntStream.range(0, entries.size()).forEach(index -> {
            val entry = entries.get(index);

            val rawName = entry.getName();
            val prefix = ChatColor.translateAlternateColorCodes('&', rawName.length() <= 16 ? "" : rawName.substring(0, 16));
            val name = formatName(rawName);

            val score = objective.getScore(name);
            score.setScore(size - index);

            val team = scoreboard.getTeam(name) == null ? scoreboard.registerNewTeam(name) : scoreboard.getTeam(name);
            team.setPrefix(prefix);
            team.addEntry(name);
        });

        updateAllEntries(player);
    }


    public void updateAllEntries(Player player) {
        entries.forEach(entry -> updateEntry(player, entry));
    }

    public ScoreboardEntry getEntryById(String id) {
        return entries.stream()
                .filter(entry -> entry.getId().toLowerCase().equals(id.toLowerCase()))
                .findFirst()
                .orElseGet(null);
    }

    public void updateEntry(Player player, ScoreboardEntry entry) {
        Preconditions.checkNotNull(player);
        Preconditions.checkNotNull(entry);
        if (ignoreScoreboard.contains(player))
            return;

        val scoreboard = player.getScoreboard();

        val rawName = entry.getName();
        val name = formatName(rawName);
        val team = scoreboard.getTeam(name);
        team.setSuffix(ChatColor.translateAlternateColorCodes('&', entry.getValue().apply(player)));
    }

    public void updateEntry(Player player, String entryId) {
        updateEntry(player, getEntryById(entryId));
    }

    private String formatName(String rawName) {
        val lastColor = rawName.length() <= 16 ? "" : ChatColor.getLastColors(ChatColor.translateAlternateColorCodes('&', rawName.substring(0, 16)));
        return ChatColor.translateAlternateColorCodes('&', rawName.length() <= 16 ? rawName : lastColor + rawName.substring(16));
    }

    @Data
    public static class ScoreboardEntry {
        private final String id;
        private final String name;
        private final Function<Player, String> value;
    }
}
